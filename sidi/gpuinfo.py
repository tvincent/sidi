import subprocess
try:
    import xmltodict
    __have_xmltodict__ = True
except ImportError:
    __have_xmltodict__ = False

def exec_shell_command(cmd, use_shell=False):
    p = subprocess.Popen(cmd.split(), shell=use_shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()
    return err, out

# TODO use pycuda.Device.get_attributes()
def get_nvidia_gpuinfo(tmp_fname="/tmp/gpuinfo.xml"):
    if not(__have_xmltodict__):
        raise ImportError("The xmltodict package is required")

    cmd = str("nvidia-smi -q -x -f %s" % tmp_fname)
    exec_shell_command(cmd)

    with open(tmp_fname) as fid:
        lines = fid.read()
    infos = xmltodict.parse(lines)
    infos = infos["nvidia_smi_log"]
    return infos
